# Taming Pandas

This is the writeup for a workshop to provide an introduction into the _Pandas_- framework.
The contents is divided in _episodes_ and _exercises_.
In the episodes it is pointed out which exercise would be suitable at which stage of progress to keep the learning and storytelling flow.

## Audience
This workshop is intended for learners interested in data science who have a basic understanding of working with Python like

* Variables, data types, functions
* Loops, comditionals

## Structure

The content is split into thematic episodes which are ordered along a story arc.
The episodes point out when to do which exercise.

The exercises themselves are split into Tasks which contain the task to be solved and a code checkpoint with the state of the workshop project before the task to make sure learners start out from the same point.

## Contact

For any inquiries about this workshop please contact [the HIFIS support](mailto:support@hifis.net)
