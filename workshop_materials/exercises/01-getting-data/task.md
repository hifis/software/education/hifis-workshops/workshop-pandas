---
title: Getting the Data
---

# Task 1: Getting the Data

The [NOAA][noaa] provides open weather data from stations around the world.
It has a [list of all weather stations][stations] and their respective codes among other info. The main part is the [weather data archive][archive]. It is sorted by year and then station code from the station list.

!!! note ""
    The station list in itself already makes for an interesting data set to explore,
    in case you are looking for practise opportunities later on.

## Tasks

1. Pick and download a sample data set from the archive.
If you are not sure, _New York, Central Park_ (Station code 725060) in 2020 would be a good starting point.
2. The data is provided in a compressed `gz`-archive, which can be extracted by most regular archive tools.
Extract the data set and visually inspect it with a text editor to make sure it does not only contain a few rows of data.
3. Get acquainted with the [ISD Lite data format][documentation], it holds valuable information how to interpret what you have in front of you.

!!! note ""
    Step 2 is not strictly necessary, _pandas_ can handle this kind of archive out of the box.
    It is however a good idea when starting out to get a first impression how the data set looks 
    and if it has a lot of missing / repeating values that might indicate a reduced usefulness.

[noaa]: https://en.wikipedia.org/wiki/National_Oceanic_and_Atmospheric_Administration
[stations]: https://www.ncei.noaa.gov/pub/data/noaa/isd-history.txt
[archive]: https://www1.ncdc.noaa.gov/pub/data/noaa/isd-lite/
[documentation]: https://www1.ncdc.noaa.gov/pub/data/noaa/isd-lite/isd-lite-format.pdf
