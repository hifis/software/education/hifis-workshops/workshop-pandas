---
title: "Initial Exploration"
---

# Task 4: Initial Exploration

Now we can start working with our data set.
Let's start by finding some basic statistical values for our location.

## Tasks
1. Find the most extreme temperatures, wind speeds and precipitation over one hour measured.
2. Calculate the years mean value for temperatures, wind speed and precipitation over one hour.
3. Find the total precipitation for the year.
4. Calculate the differences in air temperature from one hour to the next. 
Find and print the biggest rise/ drop in temperature over an hour and when they happened.
5. Add a new column to the weather data for the change in wind speed from one hour to the next.
Find and print the largest _absolute_ change in wind speed.

??? "Hints for Solving the Task"
    If you are seriously stuck, you can take a look at the [solution hints](solution.md).
