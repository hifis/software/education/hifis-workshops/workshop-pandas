---
title: "About the Exercises"
---

# About the Exercises

This is a stand-alone open-end exercise to practise working with _pandas_ on a real-live data set.

> It is highly recommended to have the [_pandas_ documentation][docs] open for this exercise, it will be needed a lot.
> Not all required functions will be listed in the exercise document, finding out what to use is part of the intended training.

Please take your time to read the instructions and hints carefully to avoid getting stuck on minor details.

[docs]: https://pandas.pydata.org/docs
