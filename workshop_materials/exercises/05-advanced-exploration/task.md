---
title: "Advanced Exploration"
---

# Advanced Tasks

Finally, let's consider more intricate questions about our data set.

## Tasks


3. Extrapolate daily statistics for from the hourly ones.
    * The columns _Temperature_, _Dew Point_, _Wind Speed_ and _Pressure_ should contain the **mean value** over the days' 24 hours
    * The columns for 1 hour and 6 hour _Precipitation_ should hold the **sum** of the days' 24 hours
    * The columns for 1 hour and 6 hour _Trace Precipitation_ should be `True` only if any of the 24 hours contained a `True` entry already. 
    * Create a new _DataFrame_ to store the daily data. It only should contain the columns mentioned above and use the day as index.
    * **Hint:** The `DataFrame.groupby(…)`-method could be of great help here.
    * **Hint:** Our index is built from `Timestamp`-objects, which have a very handy `floor(…)`-method which can be used to reduce an hourly timestamp to its day while remaining compatible with timestamps produced by `oandas.date_range(…)`.
4. **(Optional)** Come up with a metric for a "nice day" and an "awful day".
What were the most _nice_ or _awful_ days in your location?
5. **(Optional, Higher difficulty!)** Find out what the "average" wind direction in your location is. 
    * Note that wind directions can be rather fuzzy values, so you might want to come with a better metric here than simply calculating the most common value.
    * Wind directions are given in increments of 10°.
    * The wind direction wraps around after `350` to `0` after the adaptations we made when cleaning the data.
    * The approach needed here is called a [_circular mean_][circular_mean], which is somewhat tricky to understand and compute. [But maybe someone knows a good approach…][so_circular]

??? "Hints for Solving the Task"
    If you are seriously stuck, you can take a look at the [solution hints](solution.md).

[circular_mean]: https://en.wikipedia.org/wiki/Circular_mean
[so_circular]: https://stackoverflow.com/a/491907
