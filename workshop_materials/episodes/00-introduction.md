---
title: "Introduction"
---

# Introduction

## What is _pandas_?

_Pandas_ ia a framework - i.e. a collection of functionality, not a program on its own.
It is based on the numerical mathmatics framework _numpy_.

While _numpy_ is more directed to highly optimized numeric calculations, _pandas_ offers additional convenience and utility for dealing with tabular data. 
In turn, it sacrifices some processing speed.

## What is _pandas_ used for?

Its main application cases is **data processing**.
This includes:
* Reading, exploring, cleaning, transforming and visualizing data

Common areas that make use of it are:

* Data Science
* Machine Learning

## How to get _pandas_?

It can be installed via _pip_ or _conda_ ([c.f. _pandas_ on pypi.org][pandas_pypi]).
Make sure that the dependencies are installed as well.

## Where to find help?

* [Official Documentation][pandas_documentation]

!!! important "Key Points"

    * _pandas_ is a data processing framework based on _numpy_
    * It offers additional utility functions but sacrifices speed


<!-- Links !-->

[pandas_documentation]: https://pandas.pydata.org/docs/
[pandas_pypi]: https://pypi.org/project/pandas/