# Workshop - OOP in Python

## General Information

### Requirements:
* Basics

### Duration:
* Teaching: 4 hours
* Exercises: 4+ hours

This is the writeup for a workshop to provide an introduction into the _Pandas_- framework.
The contents is divided in _episodes_ and _exercises_.
In the episodes it is pointed out which exercise would be suitable at which stage of progress to keep the learning and storytelling flow.

## Audience
This workshop is intended for learners interested in data science who have a basic understanding of working with Python like

* Variables, data types, functions
* Loops, comditionals

## Structure

The content is split into thematic episodes which are ordered along a story arc.
The episodes point out when to do which exercise.
> Citations contain instructor notes

The exercises themselves are split into a `task-….md` which contains the task to be solved and a folder `checkpoint_before` with the state of the workshop project before the task and a `checkpoint_solution`-folder with the state of the workshop project after the task has been solved.

The required changes to solve the task can be highlighted by `diff`-ing the checkpoint folders.
